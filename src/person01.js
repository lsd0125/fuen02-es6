class Person {
    constructor(name='none', age=20, gender='male') {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
    toString(){
        return `${this.name}, ${this.age}, ${this.gender}`;
    }
}

export default Person;