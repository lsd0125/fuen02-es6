import Person from './person01';

class Employee extends Person {
    constructor(name='none', age=20, gender='male') {
        super(name, age, gender);
        this._employee_id = '';
    }
    get employeeId(){
        return this._employee_id;
    }
    set employeeId(val){
        this._employee_id = val;
    }
    toString() {
        return super.toString() + ', ' + this.employeeId;
    }
}

export default Employee;